
<?php
class Blog_model
{
    // private $dbh;
    // private $stmt;
    private $table = 'blog';
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAllBlog()
    {
        $this->db->query('SELECT * FROM ' . $this->table);
        return $this->db->resultSet();
        // $this->stmt = $this->dbh->prepare("SELECT * FROM blog");
        // $this->stmt->execute();
        // return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    public function getBlogById($id)
    {
        $this->db->query(' SELECT * FROM ' . $this->table . ' WHERE id=:id');
        $this->db->bind('id', $id);
        return $this->db->single();
    }
}


?>
